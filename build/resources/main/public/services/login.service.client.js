/**
 * Created by andrewdickens on 6/7/17.
 */
(function () {
    angular
        .module("facebookClone")
        .factory("LoginService", UserService);

    /**
     *
     * @param $http
     * @returns {{login: *, logout: *, findUser: findUser}}
     * @constructor
     */
    function UserService($http) {

        var api = {
            findUser: findUser,
            getFriends: getFriends,
            getPosts: getPosts
        };
        return api;

        /**
         *
         * @param user
         * @returns {*}
         */
        function findUser(user){
            return $http({
                url: '/getUserByFirstName',
                method: 'GET',
                headers: {'Content-Type': 'application/json'},
                params: {firstName : user}
            });
        }

        /**
         *
         * @param user
         * @returns {*}
         */
        function getFriends(user){
            return $http({
                url: '/getFriends',
                method: 'GET',
                headers: {'Content-Type': 'application/json'},
                params: {firstName : user}
            });
        }

        /**
         *
         * @param user
         * @returns {*}
         */
        function getPosts(user){
            return $http({
                url: '/getPosts',
                method: 'GET',
                headers: {'Content-Type': 'application/json'},
                params: {firstName : user}
            });
        }

    }
})();
