/**
 * Created by andrewdickens on 6/7/17.
 */
(function () {
    angular
        .module("facebookClone")
        .config(Config);

    /**
     *
     * @param $routeProvider
     * @constructor
     */
    function Config($routeProvider) {
        $routeProvider
            .when("/", {
                redirectTo: "/login"
            })
            .when("/login", {
                templateUrl: "views/login.view.client.html",
                controller: "LoginController",
                controllerAs: "model"
            })
            .otherwise({
                redirectTo: "/"
            });
    }
})();



