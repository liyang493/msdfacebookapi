/**
 * Created by andrewdickens on 6/7/17.
 */
(function () {
    angular
        .module("facebookClone")
        .controller("LoginController", LoginController);

    /**
     *
     * @param LoginService
     * @constructor
     */
    function LoginController(LoginService) {
        var vm = this;
        vm.findUser = findUser;
        vm.getFriends = getFriends;
        vm.getPosts = getPosts;

        /**
         *
         * @param user
         */
        function findUser(user){
            console.log("in find user, user is " + user);
            LoginService.findUser(user)
                .then(function(user){
                    console.log(user);
                    vm.result = user.data.firstName + " " + user.data.lastName;
                })
        }

        /**
         *
         * @param firstUser
         */
        function getFriends(firstUser){
            LoginService.getFriends(firstUser)
                .then(function(user){

                    if(user.data.length==0){
                        vm.result=firstUser+=" has no friends :-(";
                        return;
                    }
                    var friendString = " ";

                    for(i=0; i<user.data.length-1; i++){
                        console.log(user.data[i].firstName);
                        friendString+=user.data[i].firstName+=", ";
                    }

                    var endOfString = "and ";
                    endOfString+=user.data[user.data.length-1].firstName;
                    friendString+=endOfString;
                    friendString+=".";

                    console.log(friendString);
                    var string = firstUser+= "'s friends are: ";

                    vm.result = string+=friendString;
                })
        }

        /**
         *
         * @param firstUser
         */
        function getPosts(firstUser){
            LoginService.getPosts(firstUser)
                .then(function(post){

                    console.log(post);

                    if(post.data.length==0){
                        vm.result = firstUser+= " has no posts";
                        return;
                    }

                    var postString = " ";

                    for(i=0; i<post.data.length-1; i++){
                        postString+=post.data[i].post+=", ";
                    }

                    var endOfString = "and ";
                    endOfString+=post.data[post.data.length-1].post;
                    postString+=endOfString;
                    postString+=".";

                    var string = firstUser+= "'s posts are: ";

                    vm.result = string+=postString;                })
        }
    }
})();
