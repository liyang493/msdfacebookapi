package app;

/**
 * Created by andrewdickens on 6/3/17.
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;

@SpringBootApplication
@EnableOAuth2Sso
public class Application implements CommandLineRunner{

		/**
		 * Application requirements:
		 *
		 * Sign up for an account
		 * Connect with friends
		 * Post news to their “feeds”
		 * See the feeds of other people
		 * Create groups of friends and share content with just them
		 **/

		public static void main(String[] args) {
				SpringApplication.run(Application.class, args);
		}

		@Override
		public void run(String... strings) throws Exception {

		}
}
