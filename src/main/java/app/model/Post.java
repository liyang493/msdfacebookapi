package app.model;

/**
 * Created by andrewdickens on 6/13/17.
 */
public class Post {
		private long post_id;
		private long user_id;
		private String post;

		public Post(long post_id, long user_id, String post) {
				this.post_id = post_id;
				this.user_id = user_id;
				this.post = post;
		}

		public Post() {
		}

		public long getPost_id() {
				return post_id;
		}

		public void setPost_id(long post_id) {
				this.post_id = post_id;
		}

		public long getUser_id() {
				return user_id;
		}

		public void setUser_id(long user_id) {
				this.user_id = user_id;
		}

		public String getPost() {
				return post;
		}

		public void setPost(String post) {
				this.post = post;
		}
}
