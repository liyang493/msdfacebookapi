package app.dao;

import app.model.Post;
import app.model.User;

import java.util.List;

/**
 * Created by andrewdickens on 6/4/17.
 */
public interface UserDAO {

		/**
		 * This Interface method creates a user in the db
		 * @param firstName String representation of first name
		 * @param lastName String representation of last name
		 * @return returns success message
		 */
		public String createUser(String firstName, String lastName);

		/**
		 *
		 * @param firstName
		 * @return
		 */
		public List<User> getFriends(String firstName);

		/**
		 *
		 * @param firstName
		 * @return
		 */
		public List<Post> getPosts(String firstName);


		/**
		 * This Interface method retrieves user from db given a first name
		 * @param firstName String representation of first name
		 * @return returns success message with user infromation
		 */
		public User getUserByFirstName(String firstName);

}
