package app.dao.impl;

import app.dao.UserDAO;
import app.model.Post;
import app.model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by andrewdickens on 6/4/17.
 */
public class UserDao implements UserDAO {

		/**
		 * Fetches Spring-Datasource Bean and instantiates a datasource connection for use in this class
		 */
		ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Datasource.xml");
		SimpleDriverDataSource driverDatasource = (SimpleDriverDataSource) context.getBean("dataSource");
		JdbcTemplate jdbcTemplate = new JdbcTemplate(driverDatasource);

		/**
		 * This methods fetches a db connection and creates a user in the database
		 * @param firstName String representation of first name
		 * @param lastName String representation of last name
		 * @return returns a success message to client
		 */
		@Override public String createUser(String firstName, String lastName) {
				String sqlInsert = "INSERT INTO users (firstname, lastname)"
						+ " VALUES (?, ?)";

				jdbcTemplate.update(sqlInsert, firstName, lastName);

				return "{message : Success! User added to database}";
		}

		@Override public List<User> getFriends(String firstName) {
				User user = getUser(firstName);

				String sqlGetFriends = "select u.firstName, u.lastName from friends f LEFT JOIN users u on f.friend_id=u.user_id where f.user_id=" + user.getId();

				List<User> friends = jdbcTemplate.query(sqlGetFriends, new BeanPropertyRowMapper<User>(User.class));

				return friends;
		}

		@Override public List<Post> getPosts(String firstName) {
				User user = getUser(firstName);

				String sqlGetFriends = "select p.post from posts p LEFT JOIN users u on p.user_id=u.user_id where p.user_id=" + user.getId();

				List<Post> posts = jdbcTemplate.query(sqlGetFriends, new BeanPropertyRowMapper<Post>(Post.class));

				return posts;
		}

		/**
		 *
		 * @param firstName
		 * @return
		 */
		private User getUser(String firstName) {
				String sqlgetUID = "Select * from users where firstname like '" + firstName + "'";
				return (User) jdbcTemplate.queryForObject(sqlgetUID, new CustomRowMapper());
		}

		/**
		 * This message fetches a db connection and retrieves a user from the db given a firstName to
		 * match
		 * @param firstName String representation of first name
		 * @return return a success message with user information
		 */
		@Override public User getUserByFirstName(String firstName) {
				User user = getUser(firstName);

				return user;
		}

		/**
		 * This method is a customRowMapper customized to match userObjects (as defined in the java code)
		 * to users returned from the db.
		 */
		public class CustomRowMapper implements RowMapper{

				@Override public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						User user = new User();
						user.setId(rs.getInt("user_id"));
						user.setFirstName(rs.getString("firstName"));
						user.setLastName(rs.getString("lastName"));
						return user;
				}
		}
}
