package app.controller;

import app.dao.impl.UserDao;
import app.model.Post;
import app.model.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
public class UserControllerServer {

		/**
		 * This method adds a user to the database when /addUser is passed from the client to the server
		 * via a REST HTTP call.  This method creates a user with only the first and last name.
		 *
		 * DB Information:
		 * PostGres DB
		 * Host: msd-db-instance.cifmytiprjip.us-west-2.rds.amazonaws.com
		 * Port: 5432
		 * JDBC String: jdbc:postgresql://msd-db-instance.cifmytiprjip.us-west-2.rds.amazonaws.com:5432/postgres
		 *
		 * @param requestParams Map of the first and last names adding to the database.
		 * @return
		 */
		@RequestMapping("/addUser")
		public String addCustomer(@RequestParam Map<String,String> requestParams){

				String firstName = requestParams.get("firstName");
				String lastName = requestParams.get("lastName");

				UserDao userDao = new UserDao();
				return userDao.createUser(firstName, lastName);
		}

		/**
		 *
		 * @param requestParams
		 * @return
		 */
		@RequestMapping("/getFriends")
		public List<User> getFriends(@RequestParam Map<String,String> requestParams){

				String firstName = requestParams.get("firstName");

				UserDao userDao = new UserDao();
				return userDao.getFriends(firstName);
		}

		/**
		 *
		 * @param requestParams
		 * @return
		 */
		@RequestMapping("/getPosts")
		public List<Post> getPosts(@RequestParam Map<String,String> requestParams){

				String firstName = requestParams.get("firstName");

				UserDao userDao = new UserDao();
				return userDao.getPosts(firstName);
		}

		/**
		 * This method retrieves a user from the database where the firstName matches the param argument
		 * provided by the client via a REST HTTP call.
		 * @param requestParam firstName passed to the server by client
		 * @return
		 */
		@RequestMapping("/getUserByFirstName")
		public User getCustomerByFirstName(@RequestParam("firstName") String requestParam){

				UserDao userDao = new UserDao();
				return userDao.getUserByFirstName(requestParam);
		}
}
